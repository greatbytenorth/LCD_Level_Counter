/* 
 * File:   main.c
 * Author: david
 *
 * Created on April 1, 2013, 7:46 PM
 */

#include <stdio.h>
#include <stdlib.h>

//Configuration Bits
#include <xc.h>

// CONFIG1
#pragma config FOSC = LP    // Oscillator Selection (INTOSC oscillator: I/O function on CLKIN pin)
#pragma config WDTE = OFF       // Watchdog Timer Enable (WDT disabled)
#pragma config PWRTE = OFF      // Power-up Timer Enable (PWRT disabled)
#pragma config MCLRE = ON       // MCLR Pin Function Select (MCLR/VPP pin function is MCLR)
#pragma config CP = OFF         // Flash Program Memory Code Protection (Program memory code protection is disabled)
#pragma config CPD = OFF        // Data Memory Code Protection (Data memory code protection is disabled)
#pragma config BOREN = OFF      // Brown-out Reset Enable (Brown-out Reset disabled)
#pragma config CLKOUTEN = OFF   // Clock Out Enable (CLKOUT function is disabled. I/O or oscillator function on the CLKOUT pin)
#pragma config IESO = OFF       // Internal/External Switchover (Internal/External Switchover mode is disabled)
#pragma config FCMEN = OFF      // Fail-Safe Clock Monitor Enable (Fail-Safe Clock Monitor is disabled)

// CONFIG2
#pragma config WRT = OFF        // Flash Memory Self-Write Protection (Write protection off)
#pragma config VCAPEN = OFF     // Voltage Regulator Capacitor Enable (All VCAP pin functionality is disabled)
#pragma config PLLEN = ON       // PLL Enable (4x PLL enabled)
#pragma config STVREN = ON      // Stack Overflow/Underflow Reset Enable (Stack Overflow or Underflow will cause a Reset)
#pragma config BORV = LO        // Brown-out Reset Voltage Selection (Brown-out Reset Voltage (Vbor), low trip point selected.)
#pragma config LVP = ON         // Low-Voltage Programming Enable (Low-voltage programming enabled)


/*
 * 
 */
void main() {
	OSCCON = 0x7C;		//set internal oscillator to 8MHz

	//Set LCD Phase info and stuff
	LCDPSbits.LP = 0xF;	//set to 1:16 pre-scaler
	LCDPSbits.WFT = 0;	//Waveform type A
	LCDPSbits.BIASMD = 0;	//Set to 0 for static mode
	LCDPSbits.LCDA = 1;	//Set LCD driver module to active
	LCDPSbits.WA = 1;	//allow writing to the LCDDATAn registers

	//LCDCON settings
	LCDCONbits.LMUX = 0b00;	//set to static, only use COM0
	LCDCONbits.CS = 0b10;	//select LFINTOSC (31kHz) source
	LCDCONbits.SLPEN = 0;	//Enable LCD module in sleep mode

	//Enable LCD Segments on Pins
	LCDSE0 = 0xFF;		//Enable Segment pins 1-8
	LCDSE1 = 0xFF;		//Enable Segment pins 9-16
	LCDSE2 = 0xFF;		//Enable Segment pins 17-24

	//Write inital values to the LCDData registers
	LCDDATA0 = 0x00;
	LCDDATA1 = 0x00;
	LCDDATA2 = 0x00;

	//Clear and disable lcd interrupt
	PIR2bits.LCDIF = 0;	//clear lcd interupt bit
	PIE2bits.LCDIE = 0;	//disable lcd interrupt

	//Configure bias voltages and such
	//LCDREF register
	LCDREFbits.VLCD3PE = 1;	//enable VLCD3 pin as input

	//Turn on LCD
	LCDCONbits.LCDEN = 1;	//turn on the lcd

	//Set all segments to on
	LCDDATA0 = 0xFF;
	LCDDATA1 = 0xFF;
	LCDDATA2 = 0xFF;

	//loop forever
	while(1)
	{
		SLEEP();
	}


}

