/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>        /* For uint8_t definition */
#include <stdbool.h>       /* For true/false definition */

#include "system.h"
#include <pic.h>

/* Refer to the device datasheet for information about available
oscillator configurations. */
void ConfigureOscillator(void)
{
    /* TODO Add clock switching code if appropriate.  */
	OSCCONbits.IRCF = 0x0;	//use 32kHz osc
	//OSCCONbits.SCS = 0b00;	//use external clock source
	//OSCCONbits.SCS = 0b11l;	//use internal oscillator
    //OSCCONbits.IRCF = 0xF;      //Set to 16MHz internal oscillator
    //TODO: Change clock frequency when ready to begin low power design

    /* Typical actions in this function are to tweak the oscillator tuning
    register, select new clock sources, and to wait until new clock sources
    are stable before resuming execution of the main project. */
}
