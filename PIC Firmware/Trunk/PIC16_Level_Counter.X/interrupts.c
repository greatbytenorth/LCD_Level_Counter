/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/

#if defined(__XC)
    #include <xc.h>         /* XC8 General Include File */
#elif defined(HI_TECH_C)
    #include <htc.h>        /* HiTech General Include File */
#endif

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "user.h"

/******************************************************************************/
/* Interrupt Routines                                                         */
/******************************************************************************/

/* Baseline devices don't have interrupts. Note that some PIC16's 
 * are baseline devices.  Unfortunately the baseline detection macro is 
 * _PIC12 */
#ifndef _PIC12

void interrupt isr(void)
{

	if(UP_PIN_INT_FLAG == 1)	//if RB0 generated the interrupt
	{
		UP_PIN_INT_FLAG = 0;
		setUpButtonFlag();
                LCDCONbits.LCDEN = 1;	//turn on the lcd
//		delay(100);
//		if(LCDCONbits.LCDEN == 0)
//			LCDCONbits.LCDEN = 1;	//turn on the lcd
//		else
//			num2Screen(increaseLife());
	}
	if (DOWN_PIN_INT_FLAG == 1)	//if RB1 generated the interrupt
	{
		DOWN_PIN_INT_FLAG = 0;
		setDownButtonFlag();
//		delay(100);
//		if(LCDCONbits.LCDEN == 0)
//			LCDCONbits.LCDEN = 1;	//turn on the lcd
//		else
//			num2Screen(decreaseLife());
	}
	if (POWER_PIN_INT_FLAG == 1)
	{
		POWER_PIN_INT_FLAG = 0;
		setPowerButtonFlag();
//		delay(100);
//		num2Screen(resetLife());
//		if(LCDCONbits.LCDEN == 1)
//			LCDCONbits.LCDEN = 0;	//turn off the lcd
//		else
//			LCDCONbits.LCDEN = 1;	//turn on the lcd
	}
}
#endif


