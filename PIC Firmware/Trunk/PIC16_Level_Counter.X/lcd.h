/* 
 * File:   lcd.h
 * Author: davidpastl
 *
 * Created on February 24, 2013, 3:17 PM
 */

#ifndef LCD_H
#define	LCD_H

#include <pic.h>
#include "user.h"

//A list of port names for the LCD segments

//should really make this thing multiplex

//lcd pin map
#ifdef _PIC16F1934_H_
#define LCD1BC	SEG12COM0	//RA0
#define	LCD3F	SEG7COM0	//RA1
#define	LCD3G	SEG15COM0	//RA3
#define	LCD3E	SEG4COM0	//RA4
#define	LCD3D	SEG5COM0	//RA5
#define	LCD3C	SEG21COM0	//RE0
#define	LCD3B	SEG22COM0	//RE1
#define	LCD3A	SEG23COM0	//RE2
#define	LCD2C	SEG20COM0	//RD7
#define	LCD2B	SEG19COM0	//RD6
#define	LCD2A	SEG18COM0	//RD5
#define	LCD2F	SEG17COM0	//RD4
#define	LCD2G	SEG8COM0	//RC7
#define	LCD2E	SEG9COM0	//RC6
#define	LCD2D	SEG10COM0	//RC5
#endif
#ifdef _PIC16F1933_H_
#define LCD1BC	SEG3COM0	//RA0
#define	LCD3F	SEG1COM0	//RA1
#define	LCD3G	SEG2COM0	//RA3
#define	LCD3E	SEG5COM0	//RA4
#define	LCD3D	SEG4COM0	//RA5
#define	LCD3C	SEG15COM0	//RE0
#define	LCD3B	SEG7COM0	//RE1
#define	LCD3A	SEG12COM0	//RE2
#define	LCD2C	SEG13COM0	//RD7
#define	LCD2B	SEG14COM0	//RD6
#define	LCD2A	SEG0COM0	//RD5
#define	LCD2F	SEG8COM0	//RD4
#define	LCD2G	SEG9COM0	//RC7
#define	LCD2E	SEG10COM0	//RC6
#define	LCD2D	SEG11COM0	//RC5
#endif


//First character

//Second character

#endif	/* LCD_H */

